#HTML5 Game using Pulse Engine
-----------------------------
##About
Memory game where the player must match all card pairs using the fewer attempts and time

There are two modes:
- Player Mode
- Builder Mode

Builder Mode allows to create the game that will be played using Player Mode 

##Client
Client was built with HTML5 and [Pulse Engine](http://withpulse.com/)

##Server
Server was built with PHP and MySQL

##Bugs
1. After scroll the page using Firefox, all events targets become bad positioned
2. When clicking one card that is overlapping another one, the bottom one is triggered instead the clicked one

##Author
Tiago Alves

####Demo: [pulse.letiagoalves.com](http://pulse.letiagoalves.com/)
####Contact: <geral@letiagoalves.com>
