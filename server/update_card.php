<?php

require 'db_conn.php';

$card_data = $_POST;

$sql = "UPDATE card ";

$count = 0;
foreach($card_data["update"] as $column => $value) {
    $sql .= ($count++==0) ? " set " : " , ";
    $sql .= $column . " = :{$column} ";
}

$sql .= " WHERE id = :id ;";

$stmt = $conn->prepare($sql);

foreach($card_data["update"] as $column => $value) {
    $stmt->bindValue(":{$column}", $value);
}

$stmt->bindValue(":id", $card_data["id"]);

echo ($stmt->execute()) ? "updated" : "fail to update";

?>