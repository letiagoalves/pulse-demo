<?php

$response = array("suc" => false);

require 'db_conn.php';

$rs = $conn->query("SELECT * FROM score ORDER BY clicks ASC, remaining_time DESC LIMIT 10");

if($rs) {
    $highscores = $rs->fetchAll(PDO::FETCH_ASSOC);
    
    /* cast data */
    for($i=0, $ic=count($highscores); $i<$ic; $i++) {
        $highscores[$i]["pos"] = $i+1;
        //$highscores[$i]["id"] = (int) $cards[$i]["id"];
        $time = 20000 - $highscores[$i]["remaining_time"];
        $time= round($time/1000, 1);
        
        $highscores[$i]["time"] = $time . "s";
    }
    
    $response["suc"] = true;
    $response["highscores"] = $highscores;
}

echo json_encode($response);

?>