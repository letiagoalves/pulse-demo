<?php

$response = array("suc" => false);

require 'db_conn.php';

$score = $_POST;

$stmt = $conn->prepare("INSERT INTO score(name, clicks, remaining_time) values (:n, :c, :rt)");
$stmt->bindValue(":n", $score["name"], PDO::PARAM_STR);
$stmt->bindValue(":c", $score["clicks"], PDO::PARAM_STR);
$stmt->bindValue(":rt", round($score["remaining_time"]), PDO::PARAM_STR);

if($stmt->execute()) {
    $response["suc"] = true;
}

echo json_encode($response);

?>