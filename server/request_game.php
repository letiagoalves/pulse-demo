<?php

$ti = microtime(true);

$response = array("suc" => false);

require 'db_conn.php';

$rs = $conn->query("SELECT * FROM card ORDER BY id ASC");

if($rs) {
    $cards = $rs->fetchAll(PDO::FETCH_ASSOC);
    
    /* cast data */
    for($i=0, $ic=count($cards); $i<$ic; $i++) {
        $cards[$i]["id"] = (int) $cards[$i]["id"];
        $cards[$i]["pos_x"] = (int) $cards[$i]["pos_x"];
        $cards[$i]["pos_y"] = (int) $cards[$i]["pos_y"];
        $cards[$i]["vel_x"] = (int) $cards[$i]["vel_x"];
        $cards[$i]["vel_y"] = (int) $cards[$i]["vel_y"];
        $cards[$i]["value"] = (int) $cards[$i]["value"];
    }
    
    $response["suc"] = true;
    $response["cards"] = $cards;
}

$tf = microtime(true);

/* Minimun delay of 2 seconds 
$total_time = $tf-$ti;

if($total_time < 4) {
    sleep(4-$total_time);
}
*/

echo json_encode($response);

?>