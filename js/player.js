var DEBUG = false;
function log(s) { if(DEBUG && window.console) console.log(s); }


$(document).ready(function() {
    
    /* disable backspace */
    $(document).keydown(function(e) {
	var nodeName = e.target.nodeName.toLowerCase();
    
	if (e.which === 8) {
	    if ((nodeName === 'input' && e.target.type === 'text') ||
		nodeName === 'textarea') {
		// do nothing
	    } else {
		e.preventDefault();
	    }
	}
    });
    
    /* update highscores */
    (function updateHighscores(delay) {
	setTimeout(function() {
	    $.ajax({ url: "server/highscores.php", type: "post", dataType : "json", timeout : 10000 })
	    .done(function(res) { buildHighscores(res.highscores); })
	    .always(updateHighscores(30000));
	}, delay);
    })(0);
    
    buildGame();

});

function buildHighscores(scores) {
    log(scores);
    var $tbody = $("#highscores table tbody");
    $tbody.empty();
    for(var i=0; i<scores.length; i++) {
	var score = scores[i];
	var tr = "<tr>";
	tr += "<td>" + score.pos + "</td>";
	tr += "<td>" + score.name + "</td>";
	tr += "<td>" + score.clicks + "</td>";
	tr += "<td>" + score.time + "</td>";
	tr += "</tr>";
	$tbody.append(tr);
    }
}

var engine;
var bmpfont;
var sound_what;
function say_what() {
	sound_what.pause();
	sound_what.currentTime = 0;
	sound_what.play();
}


function buildGame()
{
    sound_what = $("#sound-what")[0];
    
    pulse.ready(function() {
	bmpfont = new pulse.BitmapFont({filename:'fonts/font.fnt'});
	 
	engine = new pulse.Engine({
          gameWindow: 'game',
          size : {
            width: 700,
            height: 480
          },
        });
        
        var scene_menu = new Game.scene_menu();
        engine.scenes.addScene(scene_menu);
        
        var scene_play = new Game.scene_play();
        engine.scenes.addScene(scene_play);
	
	var scene_gameover = new Game.scene_gameover();
	engine.scenes.addScene(scene_gameover);
	
	var scene_win = new Game.scene_win();
	engine.scenes.addScene(scene_win);
        
        engine.scenes.activateScene(scene_menu);
        
        scene_menu.events.bind('play', function(e){
	    var btn = e.sender;
	    btn.visible = false;
	    $.ajax( { url: "server/request_game.php", type : "get", dataType: "json" })
	    .done(function(res) {
		if(res && res.suc && res.cards) {
		    
		    scene_play.start(res.cards, 20000);
		    engine.scenes.deactivateScene(scene_menu);
		    engine.scenes.activateScene(scene_play);
		} else {
		    alert("oopps. Try again");
		}
	    })
	    .fail(function(){
		alert("oopps. Try again");
	    }).
	    always(function() {
		btn.visible = true;
	    });
	    
            
        });
        
        scene_play.events.bind('gameover', function(e) {
            engine.scenes.deactivateScene(scene_play);
            engine.scenes.activateScene(scene_gameover);
        });
	
	scene_gameover.events.bind('playagain', function(e) {
	    engine.scenes.deactivateScene(scene_gameover);
            engine.scenes.activateScene(scene_menu);
	});
	
	scene_win.events.bind('playagain', function(e) {
	    engine.scenes.deactivateScene(scene_win);
            engine.scenes.activateScene(scene_menu);
	});
        
        scene_play.events.bind('card_click', function(e) {
	    
            var card = e.sender;
	    
            if(card._extra.hidden == false) {
		
		card.hide();
		if(Game.state.active_cards[0] == card) {
		    Game.state.active_cards.splice(0,1); 
		}else if(Game.state.active_cards[1] == card) {
		    Game.state.active_cards.splice(1,1); 
		}else {
		    log("?!?!!?");
		    Game.state.active_cards = [];
		}
		
	    }else {
		
		if(Game.state.active_cards.length == 2)
		{
		    return;
		}else {
		    card.show();
		    Game.state.active_cards.push(card);
		    if(Game.state.active_cards.length == 2) {
			if(Game.state.active_cards[0]._extra.value
			   == Game.state.active_cards[1]._extra.value) {
			    /* good move */
			    log("good move");
			    say_what();
			    Game.state.active_cards[0]._extra.enabled = false;
			    Game.state.active_cards[1]._extra.enabled = false;
			    Game.state.active_cards = [];
			    if(--Game.state.remaining_pairs == 0)
			    {
				engine.scenes.deactivateScene(scene_play);
				
				/* send score to server */
				var score = { name : Game.player_name, clicks : scene_play.clicks, remaining_time : scene_play.remaining_time }
				$.ajax( { url : "server/new_score.php", type: "post", dataType : "json", data : score } )
				.done( function(res){

				    if(res && res.suc) {
					
					$.ajax({ url: "server/highscores.php", type: "post", dataType : "json", timeout : 10000 })
					.done(function(res) { buildHighscores(res.highscores); })
					
				    }
				}).fail(function(){
				    alert("Sorry. Server unreachable. Try again");
				});
				
				//alert("|FINISHED| clicks:" + scene_play.clicks + ", remaining_time: " + scene_play.remaining_time);
				var label_time = ( (20000-scene_play.remaining_time) / 1000 ).toFixed(1) + "s";
				scene_win.start(scene_play.clicks, label_time);
				
				engine.scenes.activateScene(scene_win);
			    }
			}else {
			    /* bad move */
			    log("bad move");
			}
		    }
		}
            }
	    
            card._extra.clicks++;
            scene_play.clicks++;
        });

        engine.go(60);
      
    });
}

/* Game */

var Game = {
    
    player_name : "PLAYER ",
    
    /* Game state */
    state : {
	active_card : [],
	remaining_pairs : 0	
    },
    
    /* Beginning of scenes */
    
    scene_menu : pulse.Scene.extend({
    
        init : function(params) {
            this._super(params);
            this.name = "SceneMenu";
	    
            var self = this;
        
            var layer_menu = new pulse.Layer( { name : "LayerMenu" } );
            layer_menu.anchor = {
                x: 0,
                y : 0
            };
	    
	    var bg = new pulse.Sprite({
	      src: 'img/bg1.jpg',
	      size: {
		width: 700,
		height: 480
	      }
	    });
	    bg.anchor = { x: 0, y: 0 };
	    layer_menu.addNode(bg);
            
            var btn_play = new pulse.Sprite({
		name: "BtnPlay",
		src: 'img/btn-play.jpg'
            });
            btn_play.position = { x: 440, y: 160};
            btn_play.events.bind('click', function(e){
		if(Game.player_name.length > 0) self.events.raiseEvent('play', e);
            });
            
            layer_menu.addNode(btn_play);
            
	    
	    this.name_label = new pulse.BitmapLabel( { name: "name_label", font: bmpfont, text: "Name: " + Game.player_name } );
	    this.name_label.position = {x: 440, y: 80};
	    
	    layer_menu.addNode(this.name_label);
	    
	    this.events.bind('keydown', function(e){
		if(e.keyCode == 8) {
		    if(Game.player_name.length > 0) Game.player_name = Game.player_name.substr(0, Game.player_name.length-2);
		}else if(e.keyCode > 64 && e.keyCode < 91) {
		    if(Game.player_name.length < 10)Game.player_name += e.key;
		}
		self.name_label.text = "Nome: " + Game.player_name + " ";
		log(Game.player_name);
	    });
	    
            this.addLayer(layer_menu);
        }
        
    }),
    
    scene_play : pulse.Scene.extend({
        
        init : function(params) {
            this._super(params);
	    this.name = "ScenePlay";
	    
            var _self = this;
        
	    var layer_hud = new pulse.Layer( { name : "hud" } );
	    layer_hud.anchor = {
                x: 0,
                y : 0
            };
	    
	    var bg = new pulse.Sprite({
	      src: 'img/bg2.jpg',
	      size: {
		width: 700,
		height: 480
	      }
	    });
	    bg.anchor = { x: 0, y: 0 };
	    layer_hud.addNode(bg);
	    
	    /*
            this.time_label = new pulse.CanvasLabel({
                text : '0',
                fontSize : 40
            });
            this.time_label.fillColor = '#FF0000';
            this.time_label.position = { x: 640, y: 40};
            */
	    this.time_label = new pulse.BitmapLabel( { name: "time_label", font: bmpfont, text: '0'} );
	    this.time_label.position = {x: 640, y: 40};
	    
            layer_hud.addNode(this.time_label);
            
	    /*
            this.score_label = new pulse.CanvasLabel({
                text : '0',
                fontSize : 40
            });
            this.score_label.fillColor = '#FF0000';
            this.score_label.position = { x: 40, y: 40};
            */
	    this.score_label = new pulse.BitmapLabel({ name: "score_label", font: bmpfont, text: '0'});
	    this.score_label.position = {x: 40, y: 40};
            
            layer_hud.addNode(this.score_label);
	
            var layer_play = new pulse.Layer( { name : "play" } );
            layer_play.anchor = {
                x: 0,
                y : 0
            };
     
	    this.addLayer(layer_hud);
            this.addLayer(layer_play);
	    
	    this.cards = [];
        },
        
        update : function(elapsed) {
            this._super(elapsed);
            
            if(this.remaining_time < 0) {
                this.remaining_time = 0;
                this.events.raiseEvent('gameover', 'timeout');
            }else {
                this.remaining_time -= elapsed;
                this.time_label.text = (this.remaining_time / 1000).toFixed(1) + " ";
                this.score_label.text = this.clicks + " ";
            }
        },
        
        start : function(new_cards, remaining_time) {
	    
	    var self = this;
	    
            this.remaining_time = remaining_time;
            this.clicks = 0;
	    Game.state.active_cards = [];
	    Game.state.remaining_pairs = new_cards.length/2;

	    for(var i=0; i<this.cards.length; i++) {
		log(this.cards[i]);
		this.getLayer("play").removeNode(this.cards[i]);
	    }
	    
	    this.cards = [];
            for(var j=0; j<new_cards.length; j++) {
		var card_extra = $.extend({}, new_cards[j], { clicks : 0, enabled : true, hidden : true });
                //var new_card = new Game.card( { id : j, value : j, clicks : 0, vel_x : 10, vel_y : 6, enabled : true, hidden : true, image : "card" + (j+1) + ".png" } );
		var new_card = new Game.card(card_extra);
		
		//new_card._extra.vel_x = 2;
		//new_card._extra.vel_y = 2;
		
		//new_card.position = { x: j * 120, y: j * 120 };
		new_card.position = { x: new_cards[j].pos_x, y: new_cards[j].pos_y };
		
		log(new_card.position);
		
                new_card.events.bind("click", function(e) {
                    if(e.sender._extra.enabled) {
                        self.events.raiseEvent('card_click', e);
                    }
                });
		
                this.getLayer("play").addNode(new_card);
                this.cards[j] = new_card;
            }
        }
        
    }),
    
    scene_gameover : pulse.Scene.extend({
    
        init : function(params) {
            this._super(params);
            this.name = "SceneGameover";
	    
            var self = this;
        
            var layer_gameover = new pulse.Layer( { name : "LayerGameOver" } );
            layer_gameover.anchor = {
                x: 0,
                y : 0
            };
	    
	    var bg = new pulse.Sprite({
	      src: 'img/bg3.jpg',
	      size: {
		width: 700,
		height: 480
	      }
	    });
	    bg.anchor = { x: 0, y: 0 };
	    layer_gameover.addNode(bg);
            
            var btn_menu = new pulse.Sprite({
		name: "BtnMenu",
		src: 'img/btn-menu.jpg'
            });
            btn_menu.position = { x: 440, y: 260};
            btn_menu.events.bind('click', function(e){
		self.events.raiseEvent('playagain', e);
            });
            
            layer_gameover.addNode(btn_menu);
            
	    
	    this.gameover_label = new pulse.BitmapLabel( { name: "gameover_label", font: bmpfont, text: "Game Over " } );
	    this.gameover_label.position = {x: 440, y: 180};
	    
	    layer_gameover.addNode(this.gameover_label);
	    
            this.addLayer(layer_gameover);
        }
        
    }),
    
    scene_win : pulse.Scene.extend({
    
        init : function(params) {
            this._super(params);
            this.name = "SceneWin";
	    
            var self = this;
        
            var layer_win = new pulse.Layer( { name : "LayerWin" } );
            layer_win.anchor = {
                x: 0,
                y : 0
            };
	    
	    var bg = new pulse.Sprite({
	      src: 'img/bg4.jpg',
	      size: {
		width: 700,
		height: 480
	      }
	    });
	    bg.anchor = { x: 0, y: 0 };
	    layer_win.addNode(bg);
            
            var btn_menu = new pulse.Sprite({
		name: "BtnMenu",
		src: 'img/btn-menu.jpg'
            });
            btn_menu.position = { x: 350, y: 340};
            btn_menu.events.bind('click', function(e){
		self.events.raiseEvent('playagain', e);
            });
            
            layer_win.addNode(btn_menu);
            
	    
	    this.win_label = new pulse.BitmapLabel( { name: "win_label", font: bmpfont, text: "YEAH! " } );
	    this.win_label.position = {x: 350, y: 150};
	    layer_win.addNode(this.win_label);
	    
	    this.win_clicks = new pulse.BitmapLabel( { name: "win_clicks", font: bmpfont, text: "" } );
	    this.win_clicks.position = {x: 350, y: 210};
	    layer_win.addNode(this.win_clicks);
	    
	    this.win_time = new pulse.BitmapLabel( { name: "win_time", font: bmpfont, text: "" } );
	    this.win_time.position = {x: 350, y: 260};
	    layer_win.addNode(this.win_time);
	    
            this.addLayer(layer_win);
        },
	
	start : function(clicks, time) {
	    this.win_clicks.text = "Clicks: " + clicks + " ";
	    this.win_time.text = "Time: " + time + " ";
	}
        
    }),
    
    /* End of scenes */
    
    
    /* Begin of sprites */
    card : pulse.Sprite.extend({

        init: function(extra) {
            var params = params || {};
	    params.name = "cardnr" + extra.id;
            params.src = "img/cards/cardcover.png";
      
            this._super(params);
            
            this._extra = extra;
        },
        
        update: function(elapsed) {
            this._super(elapsed);
            
	    if(!this._extra.enabled) return;
	    
            this.checkCollision();
            
            var plus_x = (this._extra.vel_x*elapsed / 400) * (this._extra.clicks+1);
            plus_x = Math.round(plus_x*100)/100
            this.position.x += plus_x;
            
            var plus_y = (this._extra.vel_y*elapsed / 400) * (this._extra.clicks+1);
            plus_y = Math.round(plus_y*100)/100
            this.position.y += plus_y;
	
        },
        
        hide: function() {
            this.texture = new pulse.Texture( { filename : "img/cards/cardcover.png" } );
            this._extra.hidden = true;
        },
        
        show: function() {
            this.texture = new pulse.Texture( { filename : "img/cards/" + this._extra.image } );
            this._extra.hidden = false;
        },
        
        checkCollision : function() {
	    var hit = false;
	    
            if(this.position.x - this.size.width/2 < 0) {
		hit = true;
		this._extra.vel_x = Math.abs(this._extra.vel_x);
	    }else if(this.position.x + this.size.width/2 > engine.size.width) {
		hit = true;
		this._extra.vel_x = Math.abs(this._extra.vel_x);
                this._extra.vel_x *= -1;
            }
	    
	    if(this.position.y - this.size.height/2 < 0) {
		hit = true;
		this._extra.vel_y = Math.abs(this._extra.vel_y);
	    }else if(this.position.y + this.size.height/2 > engine.size.height) {
		hit = true;
		this._extra.vel_y = Math.abs(this._extra.vel_y);
                this._extra.vel_y *= -1;
            }
	    
            return hit;
        }
        
    })
    
    /* End of sprites */
  
};