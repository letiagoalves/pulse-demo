var DEBUG = false;
function log(s) { if(DEBUG && window.console) console.log(s); }

function random(from,to)
{
    return Math.floor(Math.random()*(to-from+1)+from);
}

$(document).ready(function() {
    buildMaker();
});

var engine;
var bmpfont;
function buildMaker()
{
    pulse.ready(function() {
	bmpfont = new pulse.BitmapFont({filename:'fonts/font.fnt'});
	
	engine = new pulse.Engine({
          gameWindow: 'game_maker',
          size : {
            width: 700,
            height: 480
          },
        });
	
        var scene_wait = new Maker.scene_wait();
        engine.scenes.addScene(scene_wait);
        
        var scene_build = new Maker.scene_build();
        engine.scenes.addScene(scene_build);
	
        engine.scenes.activateScene(scene_wait);

        engine.go(60);
      
	/* load game state from server */
	$.ajax( { url : "server/request_game.php", type : "get", dataType : "json" } )
	.done(function(res) {
	    if(res && res.suc && res.cards) {
		scene_build.start(res.cards);
		engine.scenes.deactivateScene(scene_wait);
		engine.scenes.activateScene(scene_build);
	    } else {
		alert("oopps. Try again");
	    }
	})
	.fail(function(){
	    alert("oopps. Try again");
	}).
	always(function() {
	    
	});
	
	
	/* game events */
	
	scene_build.events.bind('card_moved', function(e) {
	    var card = e.sender;
	    log(card.position);
	    var params = {
		id : card._extra.id,
		
		update : {
		    pos_x : card.position.x,
		    pos_y : card.position.y
		}
	    };
	    $.ajax( { url : "server/update_card.php", type : "post", data : params } );
        });
	
	/* ----------- */
	
      
    });
}


var Maker = {
    
    scene_wait : pulse.Scene.extend({
    
        init : function(params) {
            this._super(params);
            
            var self = this;
        
            var layer_wait = new pulse.Layer();
            layer_wait.anchor = {
                x: 0,
                y : 0
            };
	    
	    var bg = new pulse.Sprite({
	      src: 'img/bg1.jpg',
	      size: {
		width: 700,
		height: 480
	      }
	    });
	    bg.anchor = { x: 0, y: 0 };
	    layer_wait.addNode(bg);
            
	    /*
            var label_wait = new pulse.CanvasLabel({
                text : 'A carregar jogo do servidor...',
                fontSize : 40
            });
            label_wait.fillColor = '#FF0000';
            label_wait.position = { x: 350, y: 240 };
            */
	    var label_wait = new pulse.BitmapLabel( { name: "label_wait", font: bmpfont, text: 'Loading game... ' } );
	    label_wait.position = {x: 440, y: 100};
            
            layer_wait.addNode(label_wait);
            
            this.addLayer(layer_wait);
	    
        }
        
    }),
    
    scene_build : pulse.Scene.extend({
	
	init : function(params) {
	    this._super(params);
	    
	    var self = this;
	    
	    var layer_bg = new pulse.Layer( { name : "bg" } );
            layer_bg.anchor = {
                x: 0,
                y : 0
            };
	    
	    var bg = new pulse.Sprite({
	      src: 'img/bg2.jpg',
	      size: {
		width: 700,
		height: 480
	      }
	    });
	    bg.anchor = { x: 0, y: 0 };
	    layer_bg.addNode(bg);
	    
	    this.addLayer(layer_bg);
	    
	    var layer_play = new pulse.Layer( { name : "play" } );
            layer_play.anchor = {
                x: 0,
                y : 0
            };
            
            this.addLayer(layer_play);
	    
	    this.cards = [];
	},
	
	start : function(cards) {
	    
	    var self = this;
	    
	    for(var i=0; i<this.cards.length; i++) {
		this.getLayer("play").removeNode(this.cards[i]);
	    }
	    
	    this.cards = [];
            for(var i=0; i<cards.length; i++) {
		var card_extra = $.extend({}, cards[i], { clicks : 0, enabled : true, hidden : true });
		var card = new Maker.card(card_extra);
		
		card.position = { x: cards[i].pos_x, y: cards[i].pos_y };
		
		
		//card.handleAllEvents = true;
		card.events.bind("dragstart", function(e) {
		    //pulse.info("dragstart");
		});
		
		card.events.bind("dragexit", function(e) {
		    //pulse.info("dragexit");
		});
		
		card.events.bind("dragenter", function(e) {
		    //pulse.info("dragenter");
		});
		
		card.events.bind("dragover", function(e) {
		    //pulse.info("dragover");
		});
		
		card.events.bind("dragdrop", function(e) {
		    //pulse.info("dragdrop");
		    e.sender.fixPosition();
		    self.events.raiseEvent('card_moved', e);
		});
		
		card.events.bind("itemdropped", function(e) {
		    //pulse.info("itemdropped");
		});
		
		
		card.dragDropEnabled = true;
		card.dragMoveEnabled = true;
		//card.dropAcceptEnabled = true;
		
		
                this.getLayer("play").addNode(card);
                this.cards[i] = card;
            }
	    
	    
	    $("#reset").bind("click", function() {
		for(var i=0; i<self.cards.length; i++) {
		    
		    var card = self.cards[i];
		    
		    var pos_x = (140*(i+1));
		    var pos_y = 160;
		    
		    if(i >= self.cards.length/2) {
			pos_x -= 140 * (self.cards.length/2);
			pos_y = 320;
		    }
		    
		    card.position = { x: pos_x, y: pos_y };
		    
		    var params = {
			id : card._extra.id,
			
			update : {
			    pos_x : card.position.x,
			    pos_y : card.position.y
			}
		    };
		    $.ajax( { url : "server/update_card.php", type : "post", data : params } );
		    
		}
	    });
	    
	    $("#shuffle").bind("click", function() {
		for(var i=0; i<self.cards.length; i++) {
		    
		    var card = self.cards[i];
		    
		    var pos_x = random(100, 600);
		    var pos_y = random(100, 380);
		    
		    card.position = { x: pos_x, y: pos_y };
		    
		    var params = {
			id : card._extra.id,
			
			update : {
			    pos_x : card.position.x,
			    pos_y : card.position.y
			}
		    };
		    $.ajax( { url : "server/update_card.php", type : "post", data : params } );
		    
		}
	    });
	    
        }
	
    }),
    
    
    /* Begin of sprites */
    card : pulse.Sprite.extend({

        init: function(extra) {
            var params = params || {};
            params.src = "img/cards/" + extra.image;
      
            this._super(params);
            
            this._extra = extra;

            var self = this;
        },
	
	fixPosition: function(extra) {
	    var hit = false;
	    
            if(this.position.x - this.size.width/2 < 0) {
		hit = true;
		this.position.x = this.size.width/2 + 20;
	    }else if(this.position.x + this.size.width/2 > engine.size.width) {
		hit = true;
		this.position.x = engine.size.width - this.size.width/2 - 20;
            }
	    
	    if(this.position.y - this.size.height/2 < 0) {
		hit = true;
		this.position.y = this.size.height/2 + 20;
	    }else if(this.position.y + this.size.height/2 > engine.size.height) {
		hit = true;
		this.position.y = engine.size.height - this.size.height/2 - 20;
            }
	    
            return hit;
	}
        
    })
    
}